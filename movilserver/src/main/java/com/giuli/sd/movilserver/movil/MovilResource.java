package com.giuli.sd.movilserver.movil;

import java.net.URI;
import java.util.List;
import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.giuli.sd.movilserver.exception.MovilExistException;
import com.giuli.sd.movilserver.exception.MovilNotFoundException;


@RestController
public class MovilResource {
	@Autowired
	private MovilDaoService service;
	
	@GetMapping(path="/moviles")
	public List<Movil> retrieveAll(){
		return service.findAll();
	}
	
	@GetMapping(path="/moviles/{id}")
	public Movil retrieveMovil(@PathVariable int id) {
		Movil movil = service.findOne(id);
		
		if(movil==null) {
			throw new MovilNotFoundException("id-"+id);
		}
		return movil;
	}
	
	@GetMapping(path="/moviles/{ubicacion}/{distancia}")
	public List<Movil> retrieveNearbyMovil(@PathVariable String ubicacion, @PathVariable double distancia) {
		List<Movil> nearbyMoviles = service.findNearMoviles(ubicacion, distancia);
		
		if(!Pattern.matches("[+-]?([0-9]*[.])?[0-9]+,[+-]?([0-9]*[.])?[0-9]+", ubicacion)) {
			throw new MovilExistException("Ubicacion con formato incorrecto, formato correcto 'x,y'");
		}
		if(nearbyMoviles==null) {
			throw new MovilNotFoundException("No exiten moviles cercanos");
		}
		return nearbyMoviles;
	}
	
	@PostMapping(path="/moviles")
	public ResponseEntity<Object> createMovil(@RequestBody Movil movil) {
		Movil savedMovil = service.save(movil);
		if(movil.getUbicacion()!=null) {
			throw new MovilExistException("Ubicacion no se puede actualizar aqui");
		}
		if(savedMovil==null) {
			throw new MovilExistException("Movil con id-"+movil.getId()+" ya existe");
		}
		URI location = ServletUriComponentsBuilder
			.fromCurrentRequest()
			.path("/{id}")
			.buildAndExpand(savedMovil.getId())
			.toUri();
		return ResponseEntity.created(location).build();
	}
	
	@PostMapping(path="/moviles/actualizar-ubi")
	public ResponseEntity<Object> actualizarUbiMovil(@RequestBody Movil movil) {
		Movil savedMovil = service.actualizarUbicacion(movil);
		if(!Pattern.matches("[+-]?([0-9]*[.])?[0-9]+,[+-]?([0-9]*[.])?[0-9]+", movil.getUbicacion())) {
			throw new MovilExistException("Ubicacion con formato incorrecto, formato correcto 'x,y'");
		}
		if(savedMovil==null) {
			throw new MovilExistException("Movil con id-"+movil.getId()+" no existe");
		}
		URI location = ServletUriComponentsBuilder
				.fromPath("/moviles/{id}")
				.buildAndExpand(savedMovil.getId())
				.toUri();
		return ResponseEntity.accepted().build();
	}
	
	@DeleteMapping(path="/moviles/{id}")
	public void deleteMovil(@PathVariable int id) {
		Movil movil = service.deleteById(id);
		
		if(movil==null) {
			throw new MovilNotFoundException("id-"+id);
		}
		return;
	}
	
}
