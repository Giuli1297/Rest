package com.giuli.sd.movilserver.movil;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Pattern;

import org.springframework.stereotype.Component;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

@Component
public class MovilDaoService {
	private static List<Movil> moviles = new ArrayList<Movil>();
	private static int idCount = 3;
	
	static {
		moviles.add(new Movil(1, "Celular", "15,2"));
		moviles.add(new Movil(2, "Auto", "10,2"));
		moviles.add(new Movil(3, "Relog", "12,48"));
	}
	
	public List<Movil> findAll(){
		return moviles;
	}
	
	public List<Movil> findNearMoviles(String ubi, double distancia){
		List<Movil> foundMoviles = new ArrayList<Movil>();
		if(Pattern.matches("[+-]?([0-9]*[.])?[0-9]+,[+-]?([0-9]*[.])?[0-9]+", ubi) && distancia>=0) {
			String[] tokens = ubi.split(",");
			double x1 = Double.parseDouble(tokens[0]);
			double y1 = Double.parseDouble(tokens[1]);
			Iterator<Movil> iterator = moviles.iterator();
			while (iterator.hasNext()) {
				Movil temp = iterator.next();
				if(temp.getUbicacion()!=null) {
					String[] tokens2 = temp.getUbicacion().split(",");
					double x2 = Double.parseDouble(tokens2[0]);
					double y2 = Double.parseDouble(tokens2[1]);
					double totalDistance = Math.sqrt(Math.pow(x2-x1, 2)+Math.pow(y2-y1, 2));
					if(totalDistance<=distancia) {
						foundMoviles.add(temp);
					}
				}
			}
			return foundMoviles;
		}
		return null;
	}
	
	public Movil save(Movil movil) {
		if(movil.getId()==null) {
			movil.setId(++idCount);
		}else {
			if(movil.getUbicacion()!=null) {
				return null;
			}
			Iterator<Movil> iterator = moviles.iterator();
			while (iterator.hasNext()) {
				if(iterator.next().getId()==movil.getId()) {
					return null;
				}
			}
		}
		moviles.add(movil);
		return movil;
	}
	
	public Movil actualizarUbicacion(Movil movil) {
		for (Movil movilIt : moviles) {
			if(movil.getId()==movilIt.getId()) {
				if(Pattern.matches("[+-]?([0-9]*[.])?[0-9]+,[+-]?([0-9]*[.])?[0-9]+", movil.getUbicacion())) {
					movilIt.setUbicacion(movil.getUbicacion());
					return movil;
				}
			}
		}
		return null;
	}
	
	
	public Movil findOne(int id) {
		for (Movil movil : moviles) {
			if(movil.getId()==id) {
				return movil;
			}
		}
		return null;
	}
	
	public Movil deleteById(int id) {
		Iterator<Movil> iterator = moviles.iterator();
		while (iterator.hasNext()) {
			Movil movil = iterator.next();
			if(movil.getId()==id) {
				iterator.remove();
				return movil;
			}
		}
		return null;
	}
}
