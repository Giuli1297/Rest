package com.giuli.sd.movilserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MovilserverApplication {

	public static void main(String[] args) {
		SpringApplication.run(MovilserverApplication.class, args);
	}

}
