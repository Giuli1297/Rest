package com.giuli.sd.movilserver.movil;

import java.net.URI;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.giuli.sd.movilserver.exception.MovilNotFoundException;


@RestController
public class MovilResource {
	@Autowired
	private MovilDaoService service;
	
	@GetMapping(path="/moviles")
	public List<Movil> retrieveAll(){
		return service.findAll();
	}
	
	@GetMapping(path="/moviles/{id}")
	public Movil retrieveMovil(@PathVariable int id) {
		Movil movil = service.findOne(id);
		
		if(movil==null) {
			throw new MovilNotFoundException("id-"+id);
		}
		return movil;
	}
	
	@PostMapping(path="/moviles")
	public ResponseEntity<Object> createMovil(@RequestBody Movil movil) {
		Movil savedMovil = service.save(movil);
		URI location = ServletUriComponentsBuilder
			.fromCurrentRequest()
			.path("/{id}")
			.buildAndExpand(savedMovil.getId())
			.toUri();
		return ResponseEntity.created(location).build();
	}
	
	@DeleteMapping(path="/moviles/{id}")
	public void deleteMovil(@PathVariable int id) {
		Movil movil = service.deleteById(id);
		
		if(movil==null) {
			throw new MovilNotFoundException("id-"+id);
		}
		return;
	}
	
}
