package com.giuli.sd.movilcliente;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Collections;
import java.util.Scanner;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
public class MovilclienteApplication implements CommandLineRunner {

	public static void main(String[] args) {
		SpringApplication.run(MovilclienteApplication.class, args);
	}
	
	@Override
	public void run(String... arg) throws Exception{
		System.out.println("==================\n\t\tREST CLIENT\n\t\tMovil Ubication App\n==================");
		System.out.print("\n\tElije alguna opcion\n"
				+ "\t\t1 - Registrar Movil\n"
				+ "\t\t2 - Consultar moviles cercanos a una ubicacion\n"
				+ "\t\t3 - Actualizar la ubicacion de un movil\n"
				+ "\t\t4 - Salir\n::::::");
		int choice = (new Scanner(System.in)).nextInt();
		while(choice<=3 && choice>0) {
			switch(choice) {
				case 1:
					System.out.print("\nRegistre el ID: ");
					int idx = (new Scanner(System.in)).nextInt();
					System.out.print("\nRegistre el Tipo: ");
					String type = (new Scanner(System.in)).nextLine();
					ResponseEntity<Movil> responseMovil = registrarMovil(idx, type);
					if(responseMovil.getStatusCode()==HttpStatus.CREATED) {
						System.out.println("\n\nMovil registrado");
					}else {
						System.out.println("ERROR");
					}
					break;
				case 2:
					System.out.print("\nSeleccione Ubicacion(x,y): ");
					String ubix = (new Scanner(System.in)).nextLine();
					System.out.print("\nSeleccion Radio: ");
					int radiox = (new Scanner(System.in)).nextInt();
					ResponseEntity<Movil[]> response = getNearMoviles(ubix, radiox);
					if(response.getStatusCode()==HttpStatus.OK) {
						for(Movil movil: response.getBody()) {
							System.out.println(movil.getId()+" "+movil.getTipo()+" "+movil.getUbicacion());
						}
					}else {
						System.out.println("Error");
					}
					break;
				case 3:
					System.out.print("\nSeleccione el ID: ");
					int idxx = (new Scanner(System.in)).nextInt();
					System.out.print("\nUbicacion(x,y): ");
					String ubixx = (new Scanner(System.in)).nextLine();
					ResponseEntity<Movil> responseActualizarMovil = actualizarUbiMovil(idxx, ubixx);
					if(responseActualizarMovil.getStatusCode()==HttpStatus.ACCEPTED) {
						System.out.println("\n\nUbicacion Actualizada");
					}else {
						System.out.println("ERROR");
					}
					break;
				default:
					System.out.println("ERROR");
					break;
			}
			System.out.print("\n\tElije alguna opcion\n"
					+ "\t\t1 - Registrar Movil\n"
					+ "\t\t2 - Consultar moviles cercanos a una ubicacion\n"
					+ "\t\t3 - Actualizar la ubicacion de un movil\n"
					+ "\t\t4 - Salir\n::::::");
			choice = (new Scanner(System.in)).nextInt();
		}
	}
	
	public ResponseEntity<Movil[]> getNearMoviles(String ubi, int radio){
		RestTemplate restTemplate = new RestTemplate();
		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
		String resourceURL = "http://localhost:8080/moviles/"+ubi+"/"+radio;
		HttpEntity<String> entity = new HttpEntity<String>(headers);
		ResponseEntity<Movil[]> response = restTemplate.exchange(resourceURL, HttpMethod.GET, entity, Movil[].class);
		return response;
	}
	
	public ResponseEntity<Movil> registrarMovil(int id, String type) throws URISyntaxException{
		RestTemplate restTemplate = new RestTemplate();
		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
		URI resourceURL = new URI("http://localhost:8080/moviles/");
		HttpEntity<String> entity = new HttpEntity<String>(headers);
		RequestEntity<Movil> request = new RequestEntity<Movil>(new Movil(id, type, null), headers, HttpMethod.POST, resourceURL, Movil.class);
		ResponseEntity<Movil> response = restTemplate.exchange(request, Movil.class);
		return response;
	}
	
	public ResponseEntity<Movil> actualizarUbiMovil(int id, String ubi) throws URISyntaxException{
		RestTemplate restTemplate = new RestTemplate();
		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
		URI resourceURL = new URI("http://localhost:8080/moviles/actualizar-ubi");
		HttpEntity<String> entity = new HttpEntity<String>(headers);
		RequestEntity<Movil> request = new RequestEntity<Movil>(new Movil(id, null, ubi), headers, HttpMethod.POST, resourceURL, Movil.class);
		ResponseEntity<Movil> response = restTemplate.exchange(request, Movil.class);
		return response;
	}
}
