package com.giuli.sd.movilcliente;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Movil {
	@JsonProperty("id")
	private Integer id;
	@JsonProperty("tipo")
	private String tipo;
	@JsonProperty("ubicacion")
	private String ubicacion;
	
	protected Movil() {}
	
	public Movil(Integer id, String tipo, String ubicacion) {
		super();
		this.id = id;
		this.tipo = tipo;
		this.ubicacion = ubicacion;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public String getUbicacion() {
		return ubicacion;
	}

	public void setUbicacion(String ubicacion) {
		this.ubicacion = ubicacion;
	}

	@Override
	public String toString() {
		return "Movil [id=" + id + ", tipo=" + tipo + ", ubicacion=" + ubicacion + "]";
	}
	
	
}

